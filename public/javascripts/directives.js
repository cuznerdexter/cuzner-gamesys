angular.module('angapp.directives', [])

.directive('gsysPromoPanel', ["PromoPanelService", "$templateRequest", "$compile", function(PromoPanelService, $templateRequest, $compile){
	return {
		link: function(scope, elem){
			$templateRequest("/templates/promo.html").then(function(html){
				var template = angular.element(html);
				elem.append(template);
				$compile(template)(scope);

			});
		}
	};
}])

.directive('gsysProductCard', ["ItemsService",  "$compile", function(ItemsService, $compile) {
	return {
		templateUrl: 'templates/card.html', 
		
	};
}])

.directive('gsysInfoLink', ["$compile", "$animate", function($compile, $animate){
	return function (scope, elem, attrs) {
		elem.bind('click', function() {
			var rootParentCont = elem[0].parentElement.parentElement.parentElement.parentElement;
			if( scope.$parent.activeInfo == false ) {
				scope.$parent.activeInfo = true;
				$animate.enter( $compile('<gsys-info-section ng-class="panelIsOpen" class="infoSection"></gsys-info-section>')(scope), angular.element(rootParentCont) , angular.element(rootParentCont) );
				elem.parent().parent().css('z-index', 15);
				scope.$parent.panelIsOpen = true;

			}
		});
	};
}])

.directive('gsysInfoSection', ["$compile", "$animate", "$timeout", "$window", "$document", function($compile, $animate, $timeout, $window, $document ) {
	
	return {
		templateUrl: 'templates/cardinfo.html',
		link: function (scope, elem, attr) {
			//var infoElem = '<h1 class="infoSection" ng-class="infoSection">DEX TEST<button ng-click="removeInfoSection()">close</button></h1>';
			var headerHeight = angular.element( document.querySelector('header') );
			var contentHeight = angular.element( document.querySelector('#contentArea') );
			var footerHeight = angular.element( document.querySelector('footer') );
			var currSelItem = '';

			//scope.$apply(function() {
				$timeout(function() {
					scope.$watch('contentAreaHeight', function(newVal, oldVal) {
						scope.contentAreaHeight = document.body.scrollHeight + 'px';
					});
				},10);
			//});
			

			//scope.$apply(function() {
				$timeout(function() {

					angular.element(elem).after('<div class="infoSectionOverlay" ng-style="{\'height\':\'contentAreaHeight\'}"></div>');		
					var infoSectionOverlay = angular.element( document.querySelector('.infoSectionOverlay') );
					var infoSection = angular.element( document.querySelector('gsys-info-section') );
					infoSectionOverlay[0].style.height = scope.contentAreaHeight;
					currSelItem = angular.element(elem);
		
					var sibling = angular.element(currSelItem[0].previousSibling );
					var siblingChild = sibling[0].children[0].children;
					currSelItem = siblingChild;

					$document.scrollToElementAnimated( infoSection, 250, 500 );
					currSelItem[0].style['pointer-events'] = 'none';

				}, 10);
			//});
			

			elem.on('$destroy', function(){
		
				$timeout(function() {
					currSelItem[0].style['z-index'] = 10;
					currSelItem[0].style['pointer-events'] = 'all';
				}, 10);
			
			});
			
		}
	};
	
	
}]);
