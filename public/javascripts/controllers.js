angular.module('angapp.controllers',[])

.controller('mainController', ['$scope', 'NavbarService', 'PromoPanelService', function($scope, NavbarService, PromoPanelService) {
	$scope.navbarData = null;
	$scope.navbarHeadData = null;
  $scope.promoData = null;
  $scope.cardData = null;
  $scope.companiesData = null;
	$scope.navbarReq =  NavbarService.getNavbarData().then(function(response){
		$scope.navbarData = response.data[0].nav;	
		$scope.navbarHeadData = response.data[1].nav;		
    $scope.cardData = response.data[2].plasticcards;
    $scope.companiesData = response.data[3].company;
	});
  $scope.promoReq = PromoPanelService.getPromoData().then(function(response){
    $scope.promoData = response.data[0].promo;
  });

  $scope.carouselInterval = 4000;
    $scope.noWrapSlides = false;
    $scope.active = 0;
    $scope.searchCountRes = 0;
    $scope.activeInfo = false;


}])


.controller('HomeController', ['$scope', function($scope){

}])

.controller('SlotsController', ['$scope', '$timeout', '$sanitize', '$animate', 'getItems', 'getCategories', 'getDetails', function($scope, $timeout, $sanitize, $animate, getItems, getCategories, getDetails){
	$scope.slotSet = getItems.data;
  $scope.quantity = 20;
	$scope.categorySet = getCategories.data;
	$scope.detailsSet = getDetails.data;

  $scope.panelIsOpen = false;

	 $scope.carouselInterval = 4000;
  	$scope.noWrapSlides = false;
  	$scope.active = 0;
  	$scope.searchCountRes = 0;
  	$scope.activeInfo = false;
  	$scope.contentAreaHeight = document.body.scrollHeight;

  	$scope.countVisibleItems = function() {
  		return $scope.active;
  	}

  	$scope.removeInfoSection = function() {
  		var infoSection = angular.element( document.querySelector('gsys-info-section') );
  		var infoOverlay = angular.element( document.querySelector('.infoSectionOverlay') );
      $scope.panelIsOpen = false;
      
      $animate.animate( 
        infoSection, 
        {  transform: 'translate3d(0px,0px,0px)' }, 
        {  transform: 'translate3d(0px,500px,0px)' })
      .then(function(){
        infoSection.remove();
        infoOverlay.remove();
      });

  		$scope.activeInfo = false;
  	}
}])

.controller('SlotsDetailController', ['$scope', function($scope){

}])

.controller('CasinoController', ['$scope', function($scope){
	
}])