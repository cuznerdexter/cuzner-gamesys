angular.module('angapp.services', [])

.factory("NavbarService", ['$http', function($http) {
	return {
		getNavbarData : function(){
			return $http({
				method: "GET",
				url: "javascripts/data-nav.json"
			});
		}
	};
}])

.factory("PromoPanelService", function($http) {
	return {
		getPromoData : function () {
			return $http({
				method: "GET",
				url: "javascripts/data-promo.json"
			});
		}
	};
})

.factory("ItemsService", function($http){
	return {
		getAll : function () {
			console.log("GET ALL ITEMS");
			return $http({
				method: "GET",
				url: "/gameitems"
			});
		},
		getCategories: function () {
			console.log("GET categories");
			return $http({
				method: "GET",
				url: "/itemcats"
			});
		},
		getDetails: function () {
			console.log("GET details");
			return $http({
				method: "GET",
				url: "/itemdetails"
			});
		}
	};
});

