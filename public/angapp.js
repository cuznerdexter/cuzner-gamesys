var angapp = angular.module('angapp', ['ui.router', 'ngSanitize', 'ngMaterial', 'ngAnimate', 'ui.bootstrap', 'duScroll', 'angapp.controllers', 'angapp.directives', 'angapp.services']);

angapp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('home', {
		url: '/home',
		controller: 'HomeController',
		templateUrl: 'templates/home.html'
	})
	
	.state('slotsinfo', {
		abstract: true,
		url: '/slots',
		views: {
			'main_content' : {
				controller: 'SlotsController',
				templateUrl: 'templates/slots.html',
			},
			'info_panel': {
				controller: 'SlotsController',
				templateUrl: 'templates/cardinfo.html'
			}
		},
		resolve: {
			getItems: function(ItemsService){
				return ItemsService.getAll();
			},
			getCategories: function(ItemsService){
				return ItemsService.getCategories();
			},
			getDetails: function(ItemsService) {
				return ItemsService.getDetails();
			}
		}
		
	})
	.state('slotsinfo.info', {
		url: '/main',
		views: {
			'info_panel': {
				controller: 'SlotsController',
				templateUrl: 'templates/cardinfo_info.html'
			}
		}
	})
	.state('slotsinfo.detail', {
		url: '/detail',
		views: {
			'info_panel': {
				controller: 'SlotsController',
				templateUrl: 'templates/cardinfo_detail.html'
			}
		}
	})

	.state('casino', {
		url: '/casino',
		controller: 'CasinoController',
		templateUrl: 'templates/casino.html'
	});
	

	$urlRouterProvider.otherwise('slots/main');
}]);