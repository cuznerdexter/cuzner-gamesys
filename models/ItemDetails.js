var mongoose = require('mongoose');

var ItemDetailSchema = new mongoose.Schema({
	info_img_url: String,
	info_descTxt: String,
	info_coinSizes: String,
	info_payLines: String,
	info_topLine: String,
	info_features: String,
	info_autoSpin: String,
	detail_bodyTxt: String,
	gameItem_id: String
});

var ItemDetail = mongoose.model('ItemDetail', ItemDetailSchema);
module.exports = ItemDetail;