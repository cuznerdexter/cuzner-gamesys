var mongoose = require('mongoose');

var ItemCategorySchema = new mongoose.Schema({
	item_cat_name: String,
	gameItem: String
});

var ItemCat = mongoose.model('ItemCat', ItemCategorySchema);
module.exports = ItemCat;