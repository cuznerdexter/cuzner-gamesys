var mongoose = require('mongoose');

var GameItemSchema = new mongoose.Schema({
	title: String,
	link: String,
	icon_url: String,
	category: { type: mongoose.Schema.Types.ObjectId, ref: 'ItemCat'},
	detail: { type: mongoose.Schema.Types.ObjectId, ref: 'ItemDetail'},
	featured: {type: Boolean, default: false}
});

var GameItem = mongoose.model('GameItem', GameItemSchema);
module.exports = GameItem;




