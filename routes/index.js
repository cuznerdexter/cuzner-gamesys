var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');


var GameItem = mongoose.model('GameItem');
var ItemDetail = mongoose.model('ItemDetail');
var ItemCat = mongoose.model('ItemCat');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/sample', function(req, res) {
    res.send('this is a sample!');  
});

/* GET ROUTES */
router.get('/gameitems', function(req, res, next) {
	GameItem.find(function(err, gameitems){
		var opts = [
			{path: 'category', model: 'ItemCat'},
			{path: 'detail', model: 'ItemDetail'}
		];
		if(err){return next(err);}
		GameItem.populate(gameitems, opts,
		function(err, gameitem){
			if(err){return next(err);}
			res.json(gameitem);
		});
	});
});


router.get('/itemcats', function(req, res, next){
	ItemCat.find(function(err, itemCategorys){
		if(err){return next(err);}
		res.json(itemCategorys);
	});
});
router.get('/itemdetails', function(req, res, next){
	ItemDetail.find(function(err, itemDetails){
		if(err){return next(err);}
		res.json(itemDetails);
	});
});


/* POST ROUTES */
// router.post('/gameitems', function(req, res, next){
// 	var gameItem = new GameItem({
// 		title: "A virgin game test"
// 	});
// 	gameItem.save(function (err) {
// 		if(err){return next(err);}
// 		var categoryItem = new ItemCategory({
// 			item_cat_name: "A virgin cat test",
// 			gameItem_id: gameitem._id
// 		});
// 		categoryItem.save(function (err) {
// 			if(err){return next(err);}
// 		});
// 	});
// });


 
module.exports = router;
